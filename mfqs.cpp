#include <bits/stdc++.h>
#include <queue>
#include <algorithm>
#include <vector>
#include <climits>
#define TQ 4
using namespace std;

typedef struct process{
	int PID;
	int arrival;
	int burst;
	int curr_burst;
	int priority;
	bool operator<(const process& rhs) const
    {
        return priority < rhs.priority;
    }
}process;

typedef struct record{
	int PID;
	int duration;
	int start;
}record;

enum estatus { C, NC };
enum sstatus { T, F };
enum eMode { RR , PQ};
queue <process> rrq;
vector <record> gnatt_chart;

estatus ES;
void gnattRecord(int ID, int start_time, int duration)
{
	record r;
	r.PID = ID;
	r.start = start_time;
	r.duration = duration;
	gnatt_chart.push_back(r);
	return;
}

/*
Exectute() : It would execute the given Process for given Time Quantum.
It would also take into consideration the next Process's Arrival Time
*/
int execute(process &p, int start_time, int nAT, int quant, sstatus status ,eMode mode)
{
	int d;
	if(start_time + quant <= nAT)
	{
		if(p.curr_burst > quant)
		{
			p.curr_burst = p.curr_burst - quant;
			if(status == T || mode == RR)
			{
				rrq.push(p);
			}
			ES = NC;
			d = quant;
		}
		else
		{
			
			d = p.curr_burst;
			p.curr_burst = 0;
			ES = C;
		}
		gnattRecord(p.PID,start_time,d);
		return d;
	}
	else{

		return execute(p, start_time, nAT, nAT - start_time, F, mode);
	}
}
bool pqcomp(process p1, process p2)
{
	return p1.priority<p2.priority ;
}
bool mycomp(process p1, process p2)
{
	return p1.arrival<p2.arrival ;
}
/*Scheduler() : Heart of the MFQS*/
void Scheduler(int N, vector <process> &processes)
{
	sort(processes.begin(),processes.end(), mycomp);
	process dummy;
	dummy.priority = INT_MAX;
	dummy.arrival = INT_MAX;
	processes.push_back(dummy);
	
	priority_queue <process/*, vector <process> , pqcomp*/> pq;
	int curr_time = 0;
	estatus PES = C;
	int pDuration=TQ, pExpected=TQ;
	curr_time = processes[0].arrival;
	ES = C;
	int i = 0;
	int nAT = 0;
	int dur;
	process* PP = NULL;
	process temp;
	process* nextToRun = NULL;
	eMode mode;
	if(processes[i].arrival > 0)
	{
		gnattRecord(-1, 0, processes[i].arrival);
	}
	while(1)
	{
		while(i<N && curr_time >= processes[i].arrival)
		{
			pq.push(processes[i]);
			i++;
		}
		nAT = processes[i+1].arrival;
		if(!pq.empty())
		{
			mode = PQ;
			if(PES == NC && pDuration < pExpected)
			{
				if(PP->priority <= pq.top().priority )
				{
					pExpected = TQ - pDuration;
					dur = execute(*PP, curr_time, nAT, pExpected, T,PQ);
					pDuration = dur;
					PES = ES;
					curr_time = curr_time + dur;
					if(PES == NC && pDuration == pExpected)
					{
						rrq.push(*PP);
					}
				}
				else
				{
					rrq.push(*PP);
					PES = C;
					pDuration = pExpected;
				}
			}
			else
			{
				temp = pq.top();
				nextToRun = &(temp);
				pq.pop();
				pExpected = TQ;
				dur = execute(temp, curr_time, nAT, TQ, T,PQ);
				pDuration = dur;
				PES = ES;
				curr_time = curr_time + dur;
				PP = nextToRun;
				/*if(PES == NC && pDuration == pExpected)
				{
					rrq.push(*PP);
				}*/
			}
		}
		else if(!rrq.empty())
		{
			temp = rrq.front();
			nextToRun = &(temp);
			dur = execute(temp, curr_time, nAT, TQ, T, RR);
			PES = C;
			pDuration = pExpected;
		}
		else if(i!=N)
		{
			gnattRecord(-1,curr_time, processes[i].arrival - curr_time);
			curr_time = processes[i].arrival;
		}
		else
		{
			break;
		}
	}
	return;
}

int main()
{
	int N,temp;
	cin>>N;
	temp = N;
	vector <process> processes;
	while(temp --){
		process p;
		cin>>p.PID;
		cin>>p.arrival;
		cin>>p.burst;
		p.curr_burst = p.burst;
		cin>>p.priority;
		processes.push_back(p);
	}
	Scheduler(N, processes);

	for(int i=0;i<gnatt_chart.size();i++)
		cout<<"("<<gnatt_chart[i].start<<" , "<<gnatt_chart[i].PID<<")"<<endl;
	return 0;
}